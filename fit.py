# -*- coding: utf-8 -*-
import sys
sys.path.append("/home/home1/friean6k/cr/util/parallel-mapper")
import time

import os
import traceback
import json

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

from util import read_particle_id, augmented_hist, load_data_file

from itertools import chain
import functools
#from joblib import Memory

import parallel

#memory = Memory('/tmp/tmpsH0V8V')

def read_from_indices (indices):

    return np.concatenate([load_data_file("data/nucleons-{}.txt".format(i)) for i in indices], axis=0)


def get_weights (mc_data, params):

    weights = mc_data["E0"]**(-simulation_p-params["p"]) #XXX: params["p"] is currently defined to be negative (ie implicitly multiplied by -1), while simulation_p isn't

    R = mc_data["E0"] / Z #NOTE: since E0 is in EeV, R is implicitly in units 1e18 as well
    #... which is handy, because particle_Rmax_base is in EV too, so we don't need to convert here.
    weights[R > params["gacc"]*particle_Rmax_base] = 0

    weights[iP] *= params["wP"]/100.
    weights[iHe] *= params["wHe"]/100.
    weights[iCNO] *= params["wCNO"]/100.
    weights[iSi] *= params["wSi"]/100.
    weights[iFe] *= (1 - params["wP"] - params["wHe"] - params["wCNO"] - params["wSi"])/100.

    np.save("/tmp/test.npy", weights)

    return weights


def reweight (mc_data, **params):

    weights = get_weights(mc_data, params)
    hist = np.bincount(np.digitize(np.log10(mc_data["E"]), bins=bins), weights=weights, minlength=num_bins+1) * params["a"]
    hist = hist[1:] #ignore first bin, for compatibility with np.histogram (technically, we'd have to ignore the last bin as well, but in this case, that extends to infinity anyways.
    #print params["p"], params["a"]

    return hist


def reweight_massdist (mc_data, **params):

    weights = get_weights(mc_data, params)
    binned = np.digitize(np.log10(mc_data["E"]), bins=lnA_bins)
    numbers = np.bincount(binned, weights=weights, minlength=len(lnA_bins))[1:]
    sums = np.bincount(binned, weights=weights*np.log(particle_ids[1]), minlength=len(lnA_bins))[1:]

    result = sums/numbers
    result[numbers == 0] = 0.

    return result


def error_function (params):

    params = {key: value for (key, value) in zip(param_names, params)}

    if (params["wP"] < 0) or (params["wP"] > 100) \
    or (params["wHe"] < 0) or (params["wHe"] > 100) \
    or (params["wCNO"] < 0) or (params["wCNO"] > 100) \
    or (params["wP"] + params["wHe"] + params["wCNO"] > 100) \
    or (params["gacc"] > 5) or (params["gacc"] < 0.2) \
    or (not (1.8 <= params["p"] <= 4.5)):  #bounds for powell
        return np.inf
    
    avg_exp_error = (exp_spectrum[:, 2] + exp_spectrum[:, 3]) / 2
    avg_exp_lnA_error = (exp_lnA["mlnAsyslo"] + exp_lnA["mlnAsyshi"]) / 2 + exp_lnA["mlnAstat"]

    residuals = np.concatenate ((
        ((reweight(mc_data, **params) - exp_spectrum[:, 1]) / avg_exp_error)[:15], #only include first 15 values (up to 1e19.5 eV) in fit
        100*(reweight_massdist(mc_data, **params) - exp_lnA["mlnA"]) / avg_exp_lnA_error,
        ))

    cost = np.sum(residuals**2)
    return cost


def leastsq_wrapper (x0, *args, **kwargs):

    success = True
    result = None
    print("x0: {}".format(x0))
    kwargs["bounds"] = zip(*kwargs["bounds"])
    
    try:
        result = scipy.optimize.minimize(error_function, x0, method="powell", *args, **kwargs)
        success = result.success
        print(result)
    except:
        print("\n\nAn error occured:\n")
        traceback.print_exc()
        print("\n\n")
        success = False

    a = np.zeros(shape=(1,), dtype=fit_output_dtype)
    cost = error_function(result.x)
    assert cost >= 0.
    try:
        a[0] = (np.bool(success), cost) + tuple(result.x)
    except:
        traceback.print_exc()

    return a

    #return np.array([result.cost] + list(result.x))

def run_fit (*args, **kwargs):

    points = np.mgrid[2.0:2.3:2j, pmin[1]:pmax[1]:2j, pmin[2]:pmax[2]:2j, pmin[3]:pmax[3]:2j, pmin[4]:pmax[4]:2j, pmin[5]:pmax[5]:2j, pmin[6]:pmax[6]:2j].T.reshape((2**7, 7))
    points = np.array([points[75]]) #Those start parameters gave the best results in the previous fit

    parallel.map_with_dtype(leastsq_wrapper, points, num_jobs=4, outfilename="fits.npy", args=args, kwargs=kwargs, result_dtype=fit_output_dtype, result_shape=(1,))#, out_dtype=fit_output_dtype, out_shape=(1,))
    #return scipy.optimize.least_squares(*args, **kwargs)



if __name__ == "__main__":

    ##CONFIG
    param_names = ["p", "wP", "wHe", "wCNO", "wSi", "a", "gacc"] #Rmax in EV
    pmin = [1.8, 80., 1., .1, .1, 1e-4, 1.]
    pmax = [2.5, 93.9, 4, 1, 1, 1e4, 3.]

    fit_output_dtype = np.dtype("b, f8, f8, f8, f8, f8, f8, f8, f8")
    names = list(fit_output_dtype.names)
    names[0] = "success"
    names[1] = "cost"
    names[2:] = param_names
    fit_output_dtype.names = tuple(names)
    ########


    #select data files
    simulation_p = 0.0
    simulation_m = None

    with open("multiple_sources.json") as f:
        seps_registry = json.load(f)

    seps_main = [entry for entry in seps_registry if "main" in entry.get("tags", [])][0]

    with open("registry.json") as f:
        registry = json.load(f)

    #Read and filter data
    mc_data = [read_from_indices([index]) for index in range(seps_main["first_index"], seps_main["last_index"]+1)]
    print([len(data) for data in mc_data])

    #Compute "base" (that is, excluding gacc) Rmax values in a mc_data-parallel array.
    #(this approach is a little different than the one used in fit.jl, but should yield the same result
    sources = np.load("sources.npy")
    particle_Rmax_bases = []

    for (source, dataset) in zip(sources, mc_data):
        particle_Rmax_bases.append(np.array([0.138 * (source["L"]/1e38)**(3/7.)]*dataset.shape[0]))

    particle_Rmax_base = np.concatenate(particle_Rmax_bases)
    mc_data = np.concatenate(mc_data)
    particle_ids = read_particle_id(mc_data["ID"])

    filter = np.logical_and (
                            mc_data["E"] > 1e-1,
                            particle_ids[0] == "nucleus")

    mc_data = mc_data.compress (filter)
    print(mc_data.shape)
    particle_Rmax_base = particle_Rmax_base.compress(filter)
    particle_ids = read_particle_id(mc_data["ID"]) #re-compute IDs to account for modified mc_data

    print("Data file loaded.")


    #Load experimental spectrum, compute bins
    exp_spectrum = np.genfromtxt("../data/CombinedSpectrum2017.txt")
    bin_width = 0.1
    num_bins = len(exp_spectrum)
    bins = np.linspace(exp_spectrum[0][0] - bin_width/2, exp_spectrum[-1][0] + bin_width/2, num_bins+1)
    bins -= 18
    bins[-1] = np.inf


    #Load experimental lnA, compute bins
    exp_lnA = np.genfromtxt("../data/lnATables_2014/lnA_QGSJetII-04.txt", names=True)
    lnA_bins = ( exp_lnA["logE"][1:] + exp_lnA["logE"][:-1] ) / 2
    lnA_bins = np.concatenate(([exp_lnA["logE"][0] - 0.05], lnA_bins, [exp_lnA["logE"][-1] + 0.05]))
    lnA_bins -= 18
    lnA_bins[-1] = np.inf



    #pre-compute some data, including particle type selectors
    name, A, Z = read_particle_id(mc_data["ID0"])
    iP = (Z == 1) & (name == "nucleus")
    iHe = (Z == 2) & (name == "nucleus")
    iCNO = ((Z == 6) | (Z == 7) | (Z == 8)) & (name == "nucleus")
    iSi = (Z == 14) & (name == "nucleus")
    iFe = (Z == 26) & (name == "nucleus")

    #... and particle type numbers
    nP = np.count_nonzero(iP)
    nHe = np.count_nonzero(iHe)
    nCNO = np.count_nonzero(iCNO)
    nSi = np.count_nonzero(iSi)
    nFe = np.count_nonzero(iFe)


    #check if fit already exists
    with open("fits.json") as f:
        all_fits = json.load(f)

    skip = False
    for entry in all_fits:
        if (entry["p"] == simulation_p) and (entry.get("m") == simulation_m) and entry.get("lnA_fit_weight") == 0. and "[seps]" in entry.get("fit_method") and "[solar]" in entry.get("fit_method"):

            skip = True
            print("Cached version of fit with these parameters found, using that.")

            params = entry["params"]
            break

    if not skip:
        print("No appropriate fit found")
        sys.exit(0)


    #DEBUG
    total = 100.
    params["wP"] /= total
    params["wHe"] /= total
    params["wCNO"] /= total
    params["wSi"] /= total
    params["wFe"] = 1. - params["wP"] - params["wHe"] - params["wCNO"] - params["wSi"]
    params["a"] *= total
    #params["wSi"] = 0.

    param_names_ = param_names + ["wFe"]

    #... and print the parameters
    print(",\n".join(["{:>6}: {:12.4e}"]*len(param_names_)).format(*list(chain.from_iterable(zip(param_names_, map(params.__getitem__, param_names_))))))

    #PLOT
    def binstep (axes, bins, counts, *args, **kwargs):

        return axes.step(bins, np.concatenate([np.array([counts[0]]), counts]), *args, **kwargs)


    _, a = plt.subplots(2, sharex=True)
    centers = exp_spectrum[:, 0] - 18

    binstep(a[0], np.minimum(bins, 2.2), reweight(mc_data, **params)*(10**(exp_spectrum[:, 0] - 18))**2 * 1e36, label="simulation", where="pre")#, linestyle="--")
    a[0].errorbar(centers, exp_spectrum[:, 1] * (10**centers)**2 * 1e36, yerr=(exp_spectrum[:, 2]+exp_spectrum[:, 3])/2 * (10**centers)**2 * 1e36, linestyle="", label="Auger", marker=".")

    a[0].semilogy()
    weights = get_weights(mc_data, params)
    a[0].hist(np.log10(mc_data["E"][iP]), weights=params["a"]*weights[iP] *mc_data["E"][iP]**2 * 1e36, log=True, label="P", bins=bins, histtype="step")
    a[0].hist(np.log10(mc_data[iHe]["E"]), weights=params["a"]*weights[iHe]*mc_data["E"][iHe]**2 * 1e36, log=True, label="He", bins=bins, histtype="step")
    a[0].hist(np.log10(mc_data[iCNO]["E"]), weights=params["a"]*weights[iCNO]*mc_data["E"][iCNO]**2 * 1e36, log=True, label="CNO", bins=bins, histtype="step")
    a[0].hist(np.log10(mc_data[iFe]["E"]), weights=params["a"]*weights[iFe]*mc_data["E"][iFe]**2 * 1e36, log=True, label="Fe", bins=bins, histtype="step")
    #a[0].hist(np.log10(mc_data[iSi]["E"]), weights=params["a"]*weights[iSi]*mc_data["E"][iSi]**2 * 1e36, log=True, label="Si", bins=bins, histtype="step")

    a[0].set_ylabel(u"E³ J / (eV² m⁻² s⁻¹ sr⁻¹)")
    a[0].legend(loc="lower left", ncol=3)

    lnA_error = np.sqrt(exp_lnA["mlnAstat"]**2  +  ((abs(exp_lnA["mlnA"] - exp_lnA["mlnAsyslo"]) + abs(exp_lnA["mlnA"] - exp_lnA["mlnAsyshi"])) / 2)**2 )
    binstep(a[1], np.minimum(lnA_bins, 1.7), reweight_massdist(mc_data, **params), label="simulation", where="pre")
    a[1].errorbar(exp_lnA["logE"]-18, exp_lnA["mlnA"], yerr=lnA_error, label="Auger", linestyle="", marker=".")
    a[1].set_xlabel(u"log (E / EeV)")
    a[1].set_ylabel(u"$\\langle \\ln A\\rangle$")

    plt.show()
    #weights = get_weights(mc_data, params)
    #augmented_hist(np.log10(mc_data["E"][iFe]), weights=weights[iFe], bins=40, histtype="step")
    #plt.semilogy(nonposy="clip")
    #plt.show()
