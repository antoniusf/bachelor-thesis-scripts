import json
import time
import os.path
import stat
import sys
import git

class Registry (object):

    def __init__ (self, base_dir):

        self.base_dir = os.path.abspath (os.path.expandvars (os.path.expanduser(base_dir)))
        self.registry_path = os.path.join(self.base_dir, "registry.json")
        self.script_path = os.path.abspath (sys.argv[0])
        assert(os.path.commonprefix([self.base_dir, self.script_path]) == self.base_dir)

        #initialize git repo
        self.repo = git.Repo(self.base_dir)
        if "registry" not in self.repo.heads:
            if "master" not in self.repo.heads:
                self.repo.index.commit("root commit")
            self.repo.create_head("registry")
            self.repo.heads.registry.set_commit(self.repo.heads.master.commit)

        self.tmp_index = git.index.base.IndexFile.from_tree(self.repo, self.repo.heads.registry.commit.tree)

        self.add_script(self.script_path)

        #read json
        with open(self.registry_path) as datafile:
            self.data = json.load(datafile)

        self.id = len(self.data)
        self.new_data = {"files": [], "description": None, "script": self.script_path}
        self.data.append(self.new_data)


    def description (self, description):

        self.new_data["description"] = description


    def add_script (self, filename):

        assert(os.path.commonprefix([self.base_dir, filename]) == self.base_dir)
        self.tmp_index.add([filename])

        
    def outfile (self, filename, comment=None):

        self.new_data["files"].append({"filename": filename, "comment": comment})
        return "data/{}-{}.txt".format(filename, self.id)


    def p (self, p):

        self.new_data["p"] = p
        return p


    def m (self, m):

        self.new_data["m"] = m
        return m


    def set_value (self, key, value):

        self.new_data[key] = value


    def start (self):

        assert self.repo.active_branch != self.repo.heads.registry, "The registry branch must not be active when running this script"
        #commit all scripts
        tree = self.tmp_index.write_tree()
        commit = git.objects.commit.Commit.create_from_tree(self.repo, tree, "<automatic commit for simulation {}>".format(self.id), parent_commits=[self.repo.heads.registry.commit])
        self.repo.heads.registry.set_commit(commit)
        
        self.new_data["start"] = time.time()
        self.new_data["completed"] = False
        self.new_data["commit"] = self.repo.heads.registry.commit.hexsha

        with open(self.registry_path, "w") as datafile:
            json.dump(self.data, datafile)
        
        del(self.new_data)
        del(self.data)

    def done (self):

        with open(self.registry_path) as datafile:
            data = json.load(datafile)

        data[self.id]["completed"] = True
        data[self.id]["end"] = time.time()

        for file in data[self.id]["files"]:
            name = "data/{}-{}.txt".format(file["filename"], self.id)
            os.chmod(name, os.stat(name).st_mode & (~stat.S_IWRITE))

        with open(self.registry_path, "w") as datafile:
            json.dump(data, datafile, indent=2, sort_keys=True)
