import os
import numpy as np
import json

nparticles = 1000000

sources = np.load("sources.npy")
LCR = sources["L"]**(6/7.)
weights = LCR / sources["D"]**2

weights = weights / sum(weights)
Nps = [int(weight*nparticles) for weight in weights]

with open("registry.json") as f:
    registry_data = json.load(f)

first_index = len(registry_data)

print("Simulating a total of {} particles.\n".format(sum(Nps)))

for i in range(len(sources)):

    os.system("/home/home1/friean6k/.virtualenvs/crpropa/bin/python test.py {} {}".format(i, Nps[i]))


with open("registry.json") as f:
    registry_data = json.load(f)

last_index = len(registry_data) - 1


with open("multiple_sources.json") as f:
    ms_data = json.load(f)

ms_data.append({"Nps": Nps, "first_index": first_index, "last_index": last_index})

with open("multiple_sources.json", "w") as f:
    json.dump(ms_data, f, indent=4, sort_keys=True)
