module Fit

using NPZ
import JSON
import StatsBase
using BlackBoxOptim

const Emin = 1e-1 # technically, this value is set by the simulation script. practically, it's been 1e-1 for pretty much every simulation I've done – including the ones we care about here.

function read_file(filename::AbstractString)

    local column_names

    open(filename) do f
        #iterators in julia wtf
        lines = eachline(f)
        start(lines)
        first_line = next(lines, nothing)[1]

        #extract column names from first line
        first_line = lstrip(first_line)
        assert(first_line[1] == '#')
        
        column_names = split(first_line[2:end])
    end
    
    #read the data
    array = readdlm(filename)

    #write into columns
    columns = Dict{String, Array{Any, 1}}()

    for (i, name) in enumerate(column_names)
        columns[name] = array[:, i]
    end

    return columns
end


function read_particle_id(id::Array{Int})

    id = copy(id)

    is_nucleus = div.(id, Int(1e8)) .== 10

    I = rem.(id, 10)
    id .= div.(id, 10)

    A = rem.(id, 1000)
    id .= div.(id, 1000)

    Z = rem.(id, 1000)

    return (is_nucleus, A, Z)
end

mutable struct FitParameters
    p::Float64
    wP::Float64
    wHe::Float64
    wCNO::Float64
    wSi::Float64
    a::Float64
    gacc::Float64

    FitParameters() = new()
end

function FitParameters(x::Array{Float64,1})
    params = FitParameters()
    params.p, params.wP, params.wHe, params.wCNO, params.wSi, params.a, params.gacc = x
    return params
end

function FitParameters(e::Dict{String, Any})
    params = FitParameters()
    params.p = e["p"]
    params.wP = e["wP"]
    params.wHe = e["wHe"]
    params.wCNO = e["wCNO"]
    params.wSi = e["wSi"]
    params.a = e["a"]
    params.gacc = e["gacc"]
    return params
end

mutable struct MCMetaData
    iP0::BitArray{1}
    iHe0::BitArray{1}
    iCNO0::BitArray{1}
    iSi0::BitArray{1}
    iFe0::BitArray{1}

    D::Array{Float64, 1}
    ID::Array{Int, 1}
    E::Array{Float64, 1}
    ID0::Array{Int, 1}
    E0::Array{Float64, 1}

    logE::Array{Float64, 1}
    lnA::Array{Float64, 1}

    Z0::Array{Float64, 1}
    R0::Array{Float64, 1}

    sampling_p::Float64
    particle_Rmax_base::Array{Float64, 1}

    weights::Array{Float64, 1} #This is a pre-allocated array for weights to improve performance

    wPs::SubArray{Float64, 1}
    wHes::SubArray{Float64, 1}
    wCNOs::SubArray{Float64, 1}
    wSis::SubArray{Float64, 1}
    wFes::SubArray{Float64, 1}

    MCMetaData() = new()
end

function compute_weights(mcdata::MCMetaData, params::FitParameters)

    mcdata.weights .= mcdata.E0 .^ (-mcdata.sampling_p - params.p)

    mcdata.weights[mcdata.R0 .> params.gacc * mcdata.particle_Rmax_base] .= 0

    mcdata.wPs .*= params.wP / 100.
    mcdata.wHes .*= params.wHe / 100.
    mcdata.wCNOs .*= params.wCNO / 100.
    mcdata.wSis .*= params.wSi / 100.
    mcdata.wFes .*= (100 - params.wP - params.wHe - params.wCNO - params.wSi) / 100.
end

function make_spectrum(mcdata::MCMetaData, params::FitParameters)

    hist = StatsBase.fit(StatsBase.Histogram, mcdata.logE, StatsBase.weights(mcdata.weights), edges)
    return (hist.weights * params.a)::Array{Float64, 1}
end

function make_lnA(mcdata::MCMetaData, params::FitParameters)

    sums = StatsBase.fit(StatsBase.Histogram, mcdata.logE, StatsBase.weights(mcdata.weights .* mcdata.lnA), lnA_edges)
    numbers = StatsBase.fit(StatsBase.Histogram, mcdata.logE, StatsBase.weights(mcdata.weights), lnA_edges)

    result = sums.weights ./ numbers.weights
    result[numbers.weights .== 0] .= 0

    return result::Array{Float64, 1}
end

function error_function(x)

    params = FitParameters()
    (params.p, params.wP, params.wHe, params.wCNO, params.wSi, params.a, params.gacc) = x

    if params.wP + params.wHe + params.wCNO + params.wSi > 100
        return Inf
    end

    compute_weights(mcdata, params)

    avg_exp_error = (exp_spectrum["Err_up"] .+ exp_spectrum["Err_low"]) / 2
    avg_exp_lnA_error = @. sqrt(exp_lnA["mlnAstat"]^2 + ((abs(exp_lnA["mlnA"] - exp_lnA["mlnAsyslo"]) + abs(exp_lnA["mlnA"] - exp_lnA["mlnAsyshi"])) / 2) ^ 2)

    residuals::Array{Float64, 1} = vcat( ((make_spectrum(mcdata, params) .- exp_spectrum["EJ"]) ./ avg_exp_error),
                      lnA_fit_weight*((make_lnA(mcdata, params) .- exp_lnA["mlnA"]) ./ avg_exp_lnA_error))

    cost = sum(residuals .^ 2)

    return cost
end


#function recompute_cost(fits)
#    global mcdata, lnA_fit_weight
#    
#    for fit in fits[3:end]
#        params = Fit.FitParameters()
#        param_list = fit["params"]["p"], fit["params"]["wP"], fit["params"]["wHe"], fit["params"]["wCNO"], fit["params"]["wSi"], fit["params"]["a"], fit["params"]["gacc"]
#        params.p, params.wP, params.wHe, params.wCNO, params.wSi, params.a, params.gacc = param_list
#        mcdata = Fit.read_mcdata(fit["indices"][1])
#        lnA_fit_weight = fit["lnA_fit_weight"]
#        fit["params"]["cost"] = error_function(param_list) #NOTE:it is important that this runs first because it ensures that the weights for mcdata are recomputed
#        fit["params"]["lnA_cost"] = lnA_error(params)
#        fit["params"]["spectrum_cost"] = spectrum_error(params)
#    end
#
#    return fits
#end
#
#
function spectrum_error(params)
    avg_exp_error = (exp_spectrum["Err_up"] .+ exp_spectrum["Err_low"]) / 2
    return sum(((make_spectrum(mcdata, params) .- exp_spectrum["EJ"]) ./ avg_exp_error) .^ 2)
end

function lnA_error(params)
    avg_exp_lnA_error = @. sqrt(exp_lnA["mlnAstat"]^2 + ((abs(exp_lnA["mlnA"] - exp_lnA["mlnAsyslo"]) + abs(exp_lnA["mlnA"] - exp_lnA["mlnAsyshi"])) / 2) ^ 2)
    return sum(((make_lnA(mcdata, params) .- exp_lnA["mlnA"]) ./ avg_exp_lnA_error) .^ 2)
end


function read_mcdata_file(index::Int)
    
    fname = "data/nucleons-$index.txt"
    raw_data = readdlm(fname)

    #check order of columns (first line)
    first_line = open(fname) do f; readline(f); end
    row_names = split(first_line)[2:end]
    assert(row_names == ["D", "ID", "E", "ID0", "E0"])

    return raw_data
end


function make_mcmetadata(seps_main)

    println("Reading data files...")
    all_data = [read_mcdata_file(i) for i=seps_main["first_index"]:seps_main["last_index"]]
    raw_data = vcat(all_data...)
    println(" $(size(raw_data, 1)) items read in total.")

    mcdata = MCMetaData()

    mcdata.D = raw_data[:, 1]
    mcdata.ID = Array{Int}(raw_data[:, 2])
    mcdata.E = raw_data[:, 3]
    mcdata.ID0 = Array{Int}(raw_data[:, 4])
    mcdata.E0 = raw_data[:, 5]

    #compute the particle_Rmax_base
    #This is done _before_ filtering, because here we still know exactly how many particles of each source are in the array, so we can easily assign particles to sources based on that. That this has to be done is a consequence of using a single monolithic mcmetadata structure for particles from all sources. It still seemed like the simplest way to do this though.

    sources = read_file("sourcesjl.txt")
    mcdata.particle_Rmax_base = zeros(mcdata.D)

    lengths = [size(data, 1) for data in all_data]

    pos = 1

    for (sourceD, sourceL, num_particles) in zip(sources["D"], sources["L"], lengths)
        mcdata.particle_Rmax_base[pos:pos+num_particles-1] .= 0.138 * (sourceL/1e38) ^ (3/7)
        #safety check
        assert(all(abs.(mcdata.D[pos:pos+num_particles-1] .- sourceD) .< 1e-1))

        pos += num_particles
    end

    assert(all(mcdata.particle_Rmax_base .!= 0))


    is_nucleus, _, _ = read_particle_id(mcdata.ID)
    pred = @. is_nucleus & (mcdata.E > 1e-1)

    #filter everything
    mcdata.D = mcdata.D[pred]
    mcdata.ID = mcdata.ID[pred]
    mcdata.E = mcdata.E[pred]
    mcdata.ID0 = mcdata.ID0[pred]
    mcdata.E0 = mcdata.E0[pred]
    mcdata.particle_Rmax_base = mcdata.particle_Rmax_base[pred]
    is_nucleus = is_nucleus[pred]

    was_nucleus, A0, Z0 = read_particle_id(mcdata.ID0)
    mcdata.Z0 = Z0
    mcdata.R0 = mcdata.E0 ./ mcdata.Z0

    mcdata.logE = log10.(mcdata.E)
    mcdata.lnA = log.(read_particle_id(mcdata.ID)[2])


    mcdata.iP0 = @. was_nucleus & (Z0 == 1)
    mcdata.iHe0 = @. was_nucleus & (Z0 == 2)
    mcdata.iCNO0 = @. was_nucleus & ((Z0 == 6) | (Z0 == 7) | (Z0 == 8))
    mcdata.iSi0 = @. was_nucleus & (Z0 == 14)
    mcdata.iFe0 = @. was_nucleus & (Z0 == 26)

    mcdata.weights = Array{Float64, 1}(length(mcdata.D))
    mcdata.wPs = view(mcdata.weights, mcdata.iP0)
    mcdata.wHes = view(mcdata.weights, mcdata.iHe0)
    mcdata.wCNOs = view(mcdata.weights, mcdata.iCNO0)
    mcdata.wSis = view(mcdata.weights, mcdata.iSi0)
    mcdata.wFes = view(mcdata.weights, mcdata.iFe0)

    registry = JSON.parsefile("registry.json")
    mcdata.sampling_p = registry[seps_main["first_index"]+1]["p"] # 1-based indexing !!!!!1!!11!!1

    #run checks
    for (name, index) in zip(sources["NED-ID"], seps_main["first_index"]:seps_main["last_index"])
        #- source association
        assert(name == registry[index+1]["NED ID"])

        #- sampling_p is all the same
        assert(mcdata.sampling_p == registry[index+1]["p"])
    end

    return mcdata

end

function read_params(entry)

    entry_params = entry["params"]

    params = FitParameters()

    params.p = entry_params["p"]
    params.wP = entry_params["wP"]
    params.wHe = entry_params["wHe"]
    params.wCNO = entry_params["wCNO"]
    params.wSi = get(entry_params, "wSi", 0.0)
    params.gacc = entry_params["gacc"]
    params.a = entry_params["a"]

    return params
end

function write_params(index, params, optimization_result)

    compute_weights(mcdata, params)

    entry = Dict("indices" => [index],
                 "m" => nothing,
                 "p" => mcdata.sampling_p,
                 "params" => Dict("cost" => BlackBoxOptim.best_fitness(optimization_result),
                                  "spectrum_cost" => spectrum_error(params),
                                  "lnA_cost" => lnA_error(params),
                                  "p" => params.p,
                                  "wP" => params.wP,
                                  "wHe" => params.wHe,
                                  "wCNO" => params.wCNO,
                                  "wSi" => params.wSi,
                                  "wFe" => 100. - params.wP - params.wHe - params.wCNO - params.wSi,
                                  "a" => params.a,
                                  "gacc" => params.gacc),
                 "lnA_fit_weight" => lnA_fit_weight,
                 "fit_method" => "julia:BlackBoxOptim:$(optimization_result.method)[Si][seps]"
                 )
    return entry
end


function main(lnA_fit_weight_::Float64)

    global exp_spectrum, edges, exp_lnA, lnA_edges, params, mcdata, lnA_fit_weight

    lnA_fit_weight = lnA_fit_weight_

    #experimental spectrum
    const exp_spectrum = read_file("../data/CombinedSpectrum2017.txt")
    spectrum_start_index = findfirst(exp_spectrum["lgE"], 18.55)
    exp_spectrum = map(pair -> pair[1] => pair[2][spectrum_start_index:end], exp_spectrum)
    bin_width = 0.1
    num_bins = length(exp_spectrum["EJ"])
    edges = Array(linspace(exp_spectrum["lgE"][1] - bin_width/2, exp_spectrum["lgE"][end] + bin_width/2, num_bins+1))
    println(edges)
    edges -= 18
    edges[end] = Inf

    #experimental lnA
    const exp_lnA = read_file("../data/lnATables_2014/lnA_QGSJetII-04.txt")
    lnA_start_index = findfirst(exp_lnA["logE"], 18.55)
    exp_lnA = map(pair -> pair[1] => pair[2][lnA_start_index:end], exp_lnA)
    lnA_edges = (exp_lnA["logE"][2:end] .+ exp_lnA["logE"][1:end-1]) / 2
    lnA_edges = vcat(exp_lnA["logE"][1] - 0.05, lnA_edges, Inf)
    lnA_edges -= 18

    #Collect data
    seps_registry = JSON.parsefile("multiple_sources.json")
    seps_main = seps_registry[findfirst(entry -> "main" in get(entry, "tags", []), seps_registry)]
    println(seps_main)
    mcdata = make_mcmetadata(seps_main)

    #Test optimization
    result = bboptimize(error_function; SearchRange = [(1.8, 3.2), (0., 100.), (0., 100.), (0., 100.), (0., 100.), (1e-18, 1e-10), (0.2, 5.)], MaxFuncEvals=8000)

    candidate = BlackBoxOptim.best_candidate(result)

    new_params = FitParameters()
    (new_params.p, new_params.wP, new_params.wHe, new_params.wCNO, new_params.wSi, new_params.a, new_params.gacc) = candidate

    println(JSON.json(new_params))

    fits = JSON.parsefile("fits.json")
    push!(fits, write_params(nothing, new_params, result))
    open("fits.json", "w") do f; JSON.print(f, fits, 4); end

    return result

end


end
