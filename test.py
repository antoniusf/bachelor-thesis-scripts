from crpropa import *
from registry import Registry

import numpy as np
import sys

registry = Registry("~/cr/secondary")
registry.description("Simulate with discrete source positions, new simulation for each source [seps]")

# See also: https://raw.githubusercontent.com/CRPropa/CRPropa3-notebooks/master/secondaries/neutrinos.py

make_secondary_photons = False
make_secondary_neutrinos = True

m = ModuleList()
m.add(SimplePropagation(10*kpc, 10*Mpc))
m.add(Redshift())

m.add(PhotoPionProduction(CMB, make_secondary_photons, make_secondary_neutrinos))
m.add(PhotoDisintegration(CMB))
#m.add(PhotoDisintegration(EBL))
m.add(NuclearDecay(make_secondary_photons, make_secondary_neutrinos))

m.add(ElectronPairProduction(CMB))
m.add(ElectronPairProduction(IRB))
#m.add(MinimumEnergy(1e17*eV))


obs_hadrons = Observer()
obs_hadrons.add(ObserverPoint())
obs_hadrons.add(ObserverNeutrinoVeto())
out_hadrons = TextOutput(registry.outfile("nucleons"), Output.Event1D)
obs_hadrons.onDetection(out_hadrons)
m.add(obs_hadrons)

obs_neutrinos = Observer()
obs_neutrinos.add(ObserverPoint())
obs_neutrinos.add(ObserverNucleusVeto())
out_neutrinos = TextOutput(registry.outfile("neutrinos"), Output.Event1D)
obs_neutrinos.onDetection(out_neutrinos)
m.add(obs_neutrinos)


index, nparticles = int(sys.argv[1]), int(sys.argv[2])

sources = np.load("sources.npy")

max_Rmax = 5. * 0.138 * (sources["L"][index] / 1e38)**(3/7.) *1e18
print "\n\nRunning simulation for source {index} ({ned}).\n {np} particles will be simulated. The maximum source Rmax is {rmax} V.\n".format(index=index, ned=sources["NED ID"][index], np=nparticles, rmax=max_Rmax)

registry.set_value("NED ID", sources["NED ID"][index])


source = Source()
source.add(SourcePosition(sources["D"][index]*Mpc))
source.add(SourceRedshift1D())
#source.add(SourceUniform1D(0, redshift2ComovingDistance(3)))
#source.add(SourceRedshiftEvolution(registry.m(2), 0, 2))

composition = SourceComposition(1e17*eV, max_Rmax*eV, registry.p(0.0))
composition.add(1, 1, 1)
#composition.add(1, 0, 1)
composition.add(4, 2, 1)
composition.add(12, 6, 1)
composition.add(14, 7, 1)
composition.add(16, 8, 1)
composition.add(28, 14, 1)
composition.add(56, 26, 1)

source.add(composition)

registry.start()

m.setShowProgress(True)
m.run(source, nparticles, True)

registry.done()
