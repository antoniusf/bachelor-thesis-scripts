from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from pylab import *
from collections import namedtuple


def augmented_hist(x, *args, **kwargs):
    
    kwargs["histtype"] = "bar"
    bins = kwargs.get("bins", None)
    
    n, bins, patches = plt.hist(x, *args, **kwargs)

    numbers, _ = np.histogram(x, bins=bins)

    cmap = matplotlib.cm.ScalarMappable(cmap=get_cmap("viridis"), norm=matplotlib.colors.LogNorm())
    colors = cmap.to_rgba(numbers)

    for i, patch in enumerate(patches):
        patch.set_color(colors[i])
        patch.set_fill(True)

    cmap.set_array(numbers)
    plt.colorbar(cmap)


other_particles = {
        10: "electron",
        12: "electron nu",
        13: "muon",
        14: "muon nu",
        22: "photon",
    }

max_pname_length = max(map(len, other_particles.values() + ["nucleus"]))

#def read_particle_id (id):
#
#    if id//10**8 == 10: #nucleus
#
#        I = id%10
#        id //= 10
#
#        A = id%1000
#        id //= 1000
#
#        Z = id%1000
#        id //= 1000
#
#        L = id%10
#
#        return ("nucleus", A, Z)
#
#    else:
#
#        return (other_particles.get(id),)

def read_particle_id (id):

    id = id.copy()

    cond = id//10**8 != 10
    name = np.empty(len(id), dtype=np.dtype((str, max_pname_length)))
    name[np.logical_not(cond)] = ["nucleus"]*(len(cond) - np.count_nonzero(cond))
    name[cond] = [other_particles.get(int(id_x), "<unknown>") for id_x in id[cond]]

    
    I = id%10
    id //= 10

    A = id%1000
    id //= 1000

    Z = id%1000

    return name, A, Z

def load_data_file (filename):

    array = np.zeros(shape=(1,), dtype=np.dtype([("D", "f8"), ("ID", "u8"), ("E", "f8"), ("ID0", "u8"), ("E0", "f8")]))

    print("Reading data:")

    index = 0

    with open(filename) as f:
        for line in f:
            if not line.lstrip().startswith("#"):
                line = line.split("\t")
                assert len(line) == 5, "Invalid data file format: {} items found, when only 5 were expected.".format(len(line))

                if index >= array.shape[0]:
                    array.resize(array.shape[0]*2)

                array[index] = (np.float64(line[0]), np.uint64(line[1]), np.float64(line[2]), np.uint64(line[3]), np.float64(line[4]))
                #array[index]["D"] = np.float64(line[0])
                #array[index]["ID"] = np.uint64(line[1])
                #array[index]["E"] = np.float64(line[2])
                #array[index]["ID0"] = np.uint64(line[3])
                #array[index]["E0"] = np.float64(line[4])

                index += 1
                #print("{:10d} items read".format(index), end="\r")

        print("{:10d} items read.".format(index), end="\n")
        array.resize(index)

    return array


def read_from_indices (name, indices):

    return np.concatenate([load_data_file("data/{}-{}.txt".format(name, i)) for i in indices], axis=0)



def load_exp_data ():
    
    #Load experimental spectrum, compute bins
    exp_spectrum = np.genfromtxt("../data/CombinedSpectrum2017.txt")
    bin_width = 0.1
    num_bins = len(exp_spectrum)
    bins = np.linspace(exp_spectrum[0][0] - bin_width/2, exp_spectrum[-1][0] + bin_width/2, num_bins+1)
    bins -= 18
    bins[-1] = np.inf


    #Load experimental lnA, compute bins
    exp_lnA = np.genfromtxt("../data/lnATables_2014/lnA_QGSJetII-04.txt", names=True)
    lnA_bins = ( exp_lnA["logE"][1:] + exp_lnA["logE"][:-1] ) / 2
    lnA_bins = np.concatenate(([exp_lnA["logE"][0] - 0.05], lnA_bins, [exp_lnA["logE"][-1] + 0.05]))
    lnA_bins -= 18
    lnA_bins[-1] = np.inf

    return namedtuple("ExpSMC", "exp_spectrum, bins, exp_lnA, lnA_bins")(exp_spectrum, bins, exp_lnA, lnA_bins)


def test():

    ids = np.array([1000010010, 1000000010, 22])

    names, A, Z = read_particle_id (ids)

    assert((names == np.array(("nucleus", "nucleus", "photon"))).all())
    assert(A[0] == A[1] == 1)
    assert(Z[0] == 1)
    assert(Z[1] == 0)
    #assert(read_particle_id(1000010010) == ("nucleus", 1, 1))
    #assert(read_particle_id(1000000010) == ("nucleus", 1, 0))
    #assert(read_particle_id(22) == ("photon",))

if __name__ == "__main__":
    test()
