Overview
======

This is a repository containing the majority of all scripts I wrote for my bachelor's thesis, including the entire simulation and analysis code. Instead of being a separate directory, where I might have copied the most important scripts, it is a mirror of the directory I used to develop all this stuff. As such, not every file still has the most straightforward name attached to it, so it seems appropriate to give a brief explanation on what each of them does.

**`test.py`:** This is the simulation script used to set up the crpropa simulation.

**`run_multiple_sources.py`:** After it became apparent that the multiple source scenario would need to run one simulation per source, in order to obtain a separate output file for each source, this script was written to automate the process by calling `test.py` repeatedly.

**`registry.py`:** A small library used by the simulation script to automatically archive the outputs of each simulation run along with the script that generated them and a bit of metadata.

**`util.py`:** Another small library containing some utility functions.

**`fit.jl`:** The Julia script responsible for performing the fit. To store the best fit, expects to find a file named `fits.json` containing a valid JSON array, so the file should be initialized manually with the contents `[]`.

**`fit.py`:** This is the "old" fitting script, used to perform fits before they were ported to Julia to increase performance and enable a better fitting algorithm. However, it did keep its role in showing the final fits.

**`read_sources.py`:** Used to pull the relevant data out of the van Velzen catalog's text file and store it in the more easily readable `npy` format.

**`Neutrinos.ipynb`:** This notebook was originally intended to show the neutrino spectrum, and later extended to do the entire plot for the continuous scenario.

**`Neutrinos-Copy1.ipynb`:** A lazy copy of the previous notebook to adapt it and show the neutrino fluxes for the discrete scenario instead.

To run scripts for the different scenarios without too much overhead, a separate "git branch" was created for each scenario. In general, the key differences between branches are focused on `fit.jl`, and perhaps `test.py`. In this web interface, you may switch branches using the drop-down menu in the top-left, directly above the file list. It should currently show `master`. The following branches should be of interest:

**`master`,** the branch used for the discrete, unconstrained fits. It also contains the simulation script used by all discrete fits, as well as the general versions of all of the other files mentioned above.

**`solar`:** This branch only has a different `fit.jl`, which performs the constrained solar fit.

**`fit-different-m`** also contains a modified `test.py` in addition to the modified `fit.jl`, because a different simulation setup is needed here.

There is also the special branch `registry`. It is used by the registry alone to commit the simulation script for each simulation run.