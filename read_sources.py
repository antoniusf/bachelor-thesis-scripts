import numpy as np

print "Loading source catalog..."
source_distances = []
source_luminosities = []
ned_ids = []

with open("../data/vanvelzen12-v1.0.txt") as f:
    first_line = next(f)
    names = first_line[:-1].split(" ")[1:] #ignore leading hash sign
    print(names)
    D_index = names.index("D")
    class_index = names.index("class")
    Lsyn_index = names.index("Lsyn")
    NED_id_index = names.index("NED_id")

    for line in f:

        _2masx, ned_id, rest = line.split('"')
        _2masx = _2masx.rstrip()
        rest = rest.strip().split(" ")
        line = [_2masx, ned_id] + rest

        if line[class_index] in ["p", "j", "u"]:
            try:
                source_distances.append(float(line[D_index]))
                source_luminosities.append(float(line[Lsyn_index]))
                ned_ids.append(line[NED_id_index])
            except:
                print("Warning: either Lsyn ({}) or D ({}) (or both) could not be converted to floats. Skipping this line.".format(line[D_index], line[Lsyn_index]))

ned_id_dtype = "U{}".format(max([len(ned_id) for ned_id in ned_ids]))

source_distances = np.array(source_distances)
source_luminosities = np.array(source_luminosities)
ned_ids = np.array(ned_ids)

source_data = np.zeros(shape=source_distances.shape, dtype=[("D", "f8"), ("L", "f8"), ("NED ID", ned_id_dtype)])
source_data["D"] = source_distances
source_data["L"] = source_luminosities
source_data["NED ID"] = ned_ids
np.save("sources.npy", source_data)
